import decimal
import random
import re
import time

import pytest


@pytest.mark.parametrize("number", [number for number in range(0, 10)])
def test_example_1(number):
    if number % 2 == 0:
        assert False
    if number % 5 == 0:
        raise Exception('Exception')
    if number % 3 == 0:
        raise Exception('WebDriverException')


@pytest.mark.parametrize("number", [number for number in range(0, 10)])
def test_example_2(number):
    if number % 2 == 0:
        assert False
    if number % 5 == 0:
        raise Exception('Exception')
    if number % 3 == 0:
        raise Exception('WebDriverException')


@pytest.mark.parametrize("number", [number for number in range(0, 10)])
def test_example_3(number):
    if number % 2 == 0:
        assert False
    if number % 5 == 0:
        raise Exception('Exception')
    if number % 3 == 0:
        raise Exception('WebDriverException')


@pytest.mark.parametrize("number", [number for number in range(0, 10)])
def test_example_4(number):
    if number % 2 == 0:
        assert False
    if number % 5 == 0:
        raise Exception('Exception')
    if number % 3 == 0:
        raise Exception('WebDriverException')


@pytest.mark.parametrize("number", [number for number in range(0, 10)])
def test_example_5(number):
    if number % 2 == 0:
        assert False
    if number % 5 == 0:
        raise Exception('Exception')
    if number % 3 == 0:
        raise Exception('WebDriverException')


def test_part_3():
    time.sleep(2)
    i = random.randint(1, 4)
    if i % 2 == 0:
        raise Exception


def test_part_2():
    time.sleep(2)


def test_part_1():
    print(1)
